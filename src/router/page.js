/* 动态 path 匹配例子：https://github.com/vuejs/vue-router/blob/dev/examples/route-matching/app.js */
import Home from '@/views/Home/index.vue'
import Layout from '@/layouts'
import PageView from '@/layouts/pageView'

/**
 * @type {import('vue-router').RouteConfig[]}
 */
export const pageRouters = [
  {
    path: '/',
    name: '/',
    meta: { title: '主页' },
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'home',
        meta: { title: '主页' },
        component: Home,
      },
      {
        path: '/about',
        name: 'about',
        meta: { title: '关于我们' },
        component: PageView,
        redirect: '/home1',
        children: [
          {
            path: '/home1',
            name: 'home1',
            meta: { title: '主页' },
            component: Home,
          },
          {
            path: '/home2',
            name: 'home2',
            meta: { title: '关于我们' },
            component: Home,
            children: [
              {
                path: '/home3',
                name: 'home3',
                meta: { title: '主页' },
                component: Home,
              },
              {
                path: '/home4',
                name: 'home4',
                meta: { title: '关于我们' },
                component: () =>
                  import(/* webpackChunkName: "about" */ '@/views/About.vue'),
              },
            ],
          },
          {
            path: '/home5',
            name: 'home5',
            meta: { title: '关于我们' },
            component: Home,
            children: [
              {
                path: '/home6',
                name: 'home6',
                meta: { title: '主页' },
                component: Home,
              },
              {
                path: '/home7',
                name: 'home7',
                meta: { title: '关于我们' },
                component: () =>
                  import(/* webpackChunkName: "about" */ '@/views/About.vue'),
              },
            ],
          },
        ],
      },
    ],
  },
]
export default pageRouters
