/**
 * 公共路由
 * @type {import('vue-router').RouteConfig[]}
 */
export const publicRouters = [
  {
    path: '/login',
    name: '登录页',
    component: () => import('@/views/404'),
  },
  {
    path: '/*',
    name: '404',
    meta: { title: '404' },
    component: () => import(/* webpackChunkName: "error" */ '@/views/404.vue'),
  },
]
export default publicRouters
