import Vue from 'vue'

import {
  Button,
  Layout,
  Row,
  Col,
  Menu,
  Breadcrumb,
  Icon,
  Modal,
  BackTop,
  Upload,
  message,
} from 'ant-design-vue'

Vue.use(Button)
Vue.use(Layout)
Vue.use(Row)
Vue.use(Col)
Vue.use(Menu)
Vue.use(Breadcrumb)
Vue.use(Icon)
Vue.use(Modal)
Vue.use(BackTop)
Vue.use(Upload)
Vue.prototype.$message = message
