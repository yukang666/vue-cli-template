module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],

  plugins: [
    /* @PC.ant-design-vue */
    /* element-ui 按需引入，详情：https://github.com/ElementUI/babel-plugin-component */
    [
      'import',
      {
        libraryName: 'ant-design-vue',
        libraryDirectory: 'es',
        style: true,
      },
    ],

    /* @H5.vant */
    /* vant 按需引入，详情：https://github.com/ElementUI/babel-plugin-component */
    [
      'component',
      {
        libraryName: 'vant',
        style: 'style/less.js',
      },
      'vant',
    ],

    /* lodash 按需引入，详情：https://github.com/lodash/babel-plugin-lodash */
    ['lodash'],
  ],
}
